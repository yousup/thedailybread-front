import React from 'react';
import YouTube from 'react-youtube';
import LoadingWheel from './images/loading-wheel.svg';
import Parser from 'html-react-parser';

export class Post extends React.Component {
    constructor(props) {
        super(props);
		this.state = {}
    }

    formatDate = (date) => {
		let d = new Date(date);
		let monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];
		let day = d.getDate();
		let monthIndex = d.getMonth();
		let year = d.getFullYear();
		return monthNames[monthIndex] + " " + day + ", " + year;
	}


    getData = () => {
		fetch("http://34.219.171.208:5002/post/"+this.props.match.params.post)
		.then(response => {
			if (!response.ok) {
				throw Error("Network request failed")
			}
			return response
		})
		.then(d => d.json())
		.then(d => {
			this.setState({
				post: d.post,
				tabTitle: d.tabTitle,
				pageTitle: d.pageTitle
			}, () => {
			document.title = d.tabTitle
		});
		}, () => {
			this.setState({
				requestFailed: true,
			})
		})
    }
    
    componentDidMount = () => {
        this.getData();
        window.scroll(0,0);
    }

    render () {
        const loaded = this.state.post;
        return (
            <div className="container post-container">
                {!loaded && 
				<div className="loading-wheel-div">
					<img src={LoadingWheel}/>
				</div>}
                {loaded && <div className="posts-detail">
					<div>
						<div className="postTitle">
							{this.state.post.postTitle}
							<span className="post-date"> {this.formatDate(this.state.post.createdAt)}</span>
						</div>
                        {this.state.post.videoUrl && <div className="postVideo"><YouTube videoId={this.state.post.videoUrl}/></div>}
                        {this.state.post.photoUrl && <div className="postPhoto"><img src={this.state.post.photoUrl}/></div>}
						<div className="postBody">{Parser(this.state.post.postBody.replace(/<br>/g, ""))}</div>
					</div>
				</div>}
            </div>
        )
    }
}