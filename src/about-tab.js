import React from 'react';
import ReactDOM from 'react-dom';
import AboutMeImg from './images/about_me.jpg'

export class AboutTab extends React.Component {
	constructor(props) {
		super(props)
		document.title = "theDailyBread | about"
	}
	render () {
		return (
			<div className="container about-tab">
				<div className="about-me-img">
					<img src={AboutMeImg}></img>
				</div>
				<div className="about-me-text">
					<h3>I have a wide array of interests, and I enjoy writing about them.</h3>
					<div>My name is Yousup Lee. Every now and then I give a short synopsis of my life to people, and a good number of them, either friend or complete stranger, ask if I blog about these things. I used to say that I "micro-blog" with Instagram as my platform. However, Instagram is terrible for such a purpose, so I made this site. I hope you will find me entertaining.</div>
				</div>
			</div>
		)
	}
}