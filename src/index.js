import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import './index.css';
import { Footer } from './footer';
import { TravelTab } from './travel-tab';
import { HomeTab } from './home-tab';
import { AboutTab } from './about-tab';
import { ContactTab } from './contact-tab';
import { Post} from './post';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

class Page extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			currentTab: "home"
		}
		document.title = ""
	}

	handleNavbarTabClick = (link) => {
		this.setState({
			currentTab: link
		});
	}

  render() {
		return ([
			<head>
				<meta name="google-site-verification" content="uqjMsKxnxH5ieg89O0Hrco27Ef0gxY3bfDSrsbghZUQ" />
			</head>,
			<div key = "1" className="header-and-content">
				<Router>
					<div className="container text-xs-center">
						<div className="row titleBar">
							<div className="col">
								<h1><a href="/">the<span>DailyBread</span></a></h1>
							</div>
						</div>

						<nav className="navbar navbar-expand-lg navbar-light">
							<a className="navbar-brand mr-auto" href="/">the<span>DailyBread</span></a>
							<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span className="navbar-toggler-icon"></span>
							</button>

							<div className="collapse navbar-collapse" id="navbarSupportedContent">
								<ul className="nav navbar-nav mx-auto w-100 justify-content-center">
									<li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
										<Link to="/bread" className="nav-link">bread <span className="sr-only">(current)</span></Link>
									</li>
									{/* <li className="nav-item">
										<a className="nav-link bg-light2" href="#">the gear bot&trade;</a>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="#">gear reviews</a>
									</li>
									<li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
										<Link to="/travel" className="nav-link">travel</Link>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="#">atx</a>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="#">lab</a>
									</li> */}
									<li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
										<Link to="/about" className="nav-link">about</Link>
									</li>
									<li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
										<Link to="/contact" className="nav-link">contact</Link>
									</li>
								</ul>
							</div>
						</nav> 
						
						<hr className="topLine"/>

						<Route exact path="/" render={() => (<Redirect to="/bread"/>)}/>
						<Route exact path="/bread" component={HomeTab}/>
						<Route path="/post/:post" component={Post} />
						<Route path="/travel" component={TravelTab}/>
						<Route path="/about" component={AboutTab}/>
						<Route path="/contact" component={ContactTab}/>
					</div>
				</Router>
			</div>,
			<Footer key = "2" />
		])
	}
}

// ========================================

ReactDOM.render(
  <Page />,
  document.getElementById('root')
);