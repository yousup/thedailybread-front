import React from 'react';
import ReactDOM from 'react-dom';
import YouTube from 'react-youtube';
import LoadingWheel from './images/loading-wheel.svg';
import Parser from 'html-react-parser';
// import { Link } from "react-router-dom";

export class HomeTab extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			enoughTimeElapsed: false,
			requestFailed: false,
			index: true,
			detail: false,
			numTimesAlreadyLoaded: 0,
			post: null
		}
		document.title = "theDailyBread | bread"
	}

	// toggle between detailed post state vs index posts state
	detailIndexClickHandler = (post) => {
		var win = window.open("/post/"+post.postTitle.replace(RegExp(" ", "g"), "-"), '_blank');
  	win.focus();
	}

	formatDate = (date) => {
		let d = new Date(date);
		let monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];
		let day = d.getDate();
		let monthIndex = d.getMonth();
		let year = d.getFullYear();
		return monthNames[monthIndex] + " " + day + ", " + year;
	}

	getData = () => {
		// fetch("http://localhost:5002/home/"+this.state.numTimesAlreadyLoaded)
		fetch("http://34.219.171.208:5002/home/"+this.state.numTimesAlreadyLoaded)
		.then(response => {
			if (!response.ok) {
				throw Error("Network request failed")
			}
			return response
		})
		.then(d => d.json())
		.then(d => {
			this.setState({
				posts: this.state.posts ? this.state.posts.concat(d.posts) : d.posts,
				tabTitle: d.tabTitle,
				pageTitle: d.pageTitle
			});
		}, () => {
			this.setState({
				requestFailed: true,
			})
		})
	}

	handleScroll = () => {
		const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
		const body = document.body;
		const html = document.documentElement;
		const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
		const windowBottom = windowHeight + window.pageYOffset;
		if (windowBottom >= docHeight && this.state.index) { // Want this to happen only on index view
		this.setState({
					numTimesAlreadyLoaded: this.state.numTimesAlreadyLoaded + 1,
				});
				this.getData();
		} else {
		}
	}

	componentDidMount() {
		this.getData();
		window.addEventListener("scroll", this.handleScroll);
		setTimeout(() => {
			this.setState({
				enoughTimeElapsed: true,
			})
		}, 300);
	}

	componentWillUnmount() {
		// If this is not removed, multiple event listeners will be added each time a tab mounts
		window.removeEventListener("scroll", this.handleScroll);
	}

	render () {
		// Simple trick to check if this.state.data has been loaded
		const loaded = this.state.posts;
		return (
			<div className="container post-container">
				{/* 
						The following block is for index view
				*/}
				{(!loaded || !this.state.enoughTimeElapsed) && 
				<div className="loading-wheel-div">
					<img src={LoadingWheel}/>
				</div>}
				{loaded && this.state.enoughTimeElapsed && this.state.index && <div className="posts-index">
					{/* <h4 className="content-headline">latest posts</h4> */}
					{this.state.posts.map(post => 
						<div key={post.postTitle}>
							<div className="postTitle">
								{post.postTitle}
								<span className="post-date"> {this.formatDate(post.createdAt)}</span>
							</div>
							{post.videoUrl && <div className="postVideo"><YouTube videoId={post.videoUrl}/></div>}
							{post.photoUrl && <div className="postPhoto"><img src={post.photoUrl}/></div>}
							<div className="tagline">{post.tagline}</div>
							<div className="read-more-btn-div">
								<button type="button" className="btn btn-light" onClick={() => this.detailIndexClickHandler(post)}> 
									Read More
								</button>
							</div>
						</div>
					)}
				</div>}
			</div>
		)
	}
}