import React from 'react';
import ReactDOM from 'react-dom';
import LoadingWheel from './images/loading-wheel.svg';

export class ContactTab extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			name: '',
			email: '',
			subject: '',
			message: '',
			requestFailed: false
		}
		document.title = "theDailyBread | contact"
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	getData = () => {
		fetch("http://34.219.171.208:5002/email/",{
			method: "POST",
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				name: this.state.name,
				email: this.state.email,
				subject: this.state.subject,
				message: this.state.message
			})
		})
		.then(response => {
			if (!response.ok) {
				throw Error("Network request failed")
			}
			return response
		})
		.then(d => d.json())
		.then(d => {
			this.setState({
				status: d.status,
				statusMessage: d.statusMessage,
			});
		}, () => {
			this.setState({
				requestFailed: true,
			})
		})
	}
	
	handleChange = (e) => {
		let newState = {}
		newState[e.target.name] = e.target.value
		this.setState(newState)
	}

	handleSubmit = (e, message) => {
		this.setState({
			status: "sending"
		})
		e.preventDefault(); // Prevents page refresh
		this.getData();
	}

	render () {
		return (
			<div className="container contact-form-container">
				<div className="contact-form-header">
					<h4>Feedback</h4>
					<strong>Videography needs?</strong> Questions? Concerns? Comments? Fill out the form and I shall get back to you as soon as possible.
				</div>
				<form onSubmit={this.handleSubmit}>
				<div className="form-group">
						<label htmlFor="inputName">Name</label>
						<input type="text" className="form-control" id="inputName" placeholder="Name" onChange={this.handleChange}name="name" value={this.state.name}/>
					</div>
					<div className="form-group">
						<label htmlFor="inputEmail">Email address</label>
						<input type="email" className="form-control" id="inputEmail" aria-describedby="emailHelp" onChange={this.handleChange} name="email" placeholder="Email Address" value={this.state.email}/>
					</div>
					<div className="form-group">
						<label htmlFor="inputSubject">Subject</label>
						<input type="text" className="form-control" id="inputSubject" placeholder="Subject" onChange={this.handleChange} name="subject" value={this.state.subject}/>
					</div>
					<div className="form-group">
						<label htmlFor="inputMessage">Message</label>
						<textarea className="form-control" placeholder="Message" id="inputMessage" rows="4" onChange={this.handleChange} name="message" value={this.state.message}></textarea>
					</div>
					<button className="btn btn-light">Send</button>
				</form>
				<div className="contact-loading-wheel-div">
					{this.state.status === "sending" && <img src={LoadingWheel}/>}
				</div>
				<div className="email-success-message">
					{this.state.status === "sent" && <span>{this.state.statusMessage}</span>}
				</div>
				<div className="email-failed-message">
					{this.state.status === "failed" && <span>{this.state.statusMessage}</span>}
				</div>
			</div>
		)
	}
}