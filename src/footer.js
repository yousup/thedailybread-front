import React from 'react';
import ReactDom from 'react-dom';
import InstagramLink from './images/instagram-link.png';
import YoutubeLink from './images/youtube-link.png';

export class Footer extends React.Component {
	render () {
		return (
			<div className="footer-container-outer">
				<div className="container footer-container">
					<div className="social-media-links">
						<div>
							Follow me on social media!
						</div>
						<div>
							<a href="http://www.youtube.com/c/YousupLee" target="_blank"><img src={YoutubeLink}></img></a>
							<a href="https://www.instagram.com/heysupyou/" target="_blank"><img src={InstagramLink}></img></a>
						</div>
					</div>
					<div className="copyright">
						<small> © Wylee Imagery LLC and Yousup Lee. May not be used without permission.</small>
					</div>
				</div>
			</div>
		)
	}
}